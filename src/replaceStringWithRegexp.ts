import _ from "lodash";

type StringObject = {
  [key: string]: string | RegExp | StringObject;
};

type RegExpObject = {
  [key: string]: RegExp | RegExpObject;
};

export function replaceStringWithRegex(obj: StringObject): RegExpObject {
  return _.mapValues(obj, value => {
    if (_.isString(value)) {
      try {
        return new RegExp(value, "u");
      } catch (e) {
        console.warn("Invalid RegEx: ", value);
        throw e;
      }
    }

    if (_.isObjectLike(obj) && !_.isArray(obj)) {
      return replaceStringWithRegex(obj);
    }
    return value as RegExp;
  });
}
