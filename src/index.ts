import visit from "unist-util-visit";
import { parseLineNumberRange } from "./parseLineNumberRange";
import { getName, getTitle, highlightCode } from "./highlightCode";
import { addLineNumbers } from "./addLineNumbers";

type Node = { markdownAST: any };
type Aliases = { [key: string]: string };
type Options = {
  classPrefix: string;
  inlineCodeMarker?: string | null;
  noInlineHighlight: boolean;
  showLineNumbers: boolean;
  aliases: Aliases;
};

function normalizeLanguage(language: string, aliases: Aliases): string {
  const lower = language.toLowerCase();
  return aliases[lower] || getName(lower);
}

export = function (node: Node, options: Options): void {
  const { markdownAST } = node;
  const {
    classPrefix = "language-",
    inlineCodeMarker = null,
    aliases = {},
    noInlineHighlight = false,
    showLineNumbers: showLineNumbersGlobal = false
  } = options;
  visit(markdownAST, "code", (node: any) => {
    let language = node.lang;
    const {
      splitLanguage,
      highlightLines,
      showLineNumbersLocal
    } = parseLineNumberRange(language);
    const showLineNumbers = showLineNumbersLocal || showLineNumbersGlobal;
    language = splitLanguage;

    // PrismJS's theme styles are targeting pre[class*="language-"]
    // to apply its styles. We do the same here so that users
    // can apply a PrismJS theme and get the expected, ready-to-use
    // outcome without any additional CSS.
    //
    // @see https://github.com/PrismJS/prism/blob/1d5047df37aacc900f8270b1c6215028f6988eb1/themes/prism.css#L49-L54
    let languageName = "text";
    if (language) {
      languageName = normalizeLanguage(language, aliases);
    }

    const showLanguage = getTitle(languageName);

    // Allow users to specify a custom class prefix to avoid breaking
    // line highlights if Prism is required by any other code.
    // This supports custom user styling without causing Prism to
    // re-process our already-highlighted markup.
    // @see https://github.com/gatsbyjs/gatsby/issues/1486
    const className = `${classPrefix}${languageName}`;

    let numLinesClass, numLinesNumber;
    const numLinesStyle = (numLinesClass = numLinesNumber = "");
    if (showLineNumbers) {
      /*
      /numLinesStyle = ` style="counter-reset: linenumber ${numberLinesStartAt -
        1}"`;*/
      numLinesClass = " line-numbers";
      numLinesNumber = addLineNumbers(node.value);
    }

    // Replace the node with the markup we need to make
    // 100% width highlighted code lines work
    node.type = "html";

    let highlightClassName = "gatsby-highlight";
    if (highlightLines && highlightLines.length > 0) {
      highlightClassName += " has-highlighted-lines";
    }
    const code = highlightCode(languageName, node.value, highlightLines);

    const nameNode = showLanguage
      ? `<span class="language-name">${showLanguage}</span>`
      : "";
    node.value = `
    <div class="${highlightClassName}" data-language="${languageName}">
      <pre${numLinesStyle} class="${className}${numLinesClass}"><code class="${className}">${code}${numLinesNumber}</code></pre>
      ${nameNode}
    </div>
    `;
  });

  if (!noInlineHighlight) {
    visit(markdownAST, "inlineCode", (node: any) => {
      let languageName = "text";

      if (inlineCodeMarker) {
        const [language, restOfValue] = node.value.split(
          `${inlineCodeMarker}`,
          2
        );
        if (language && restOfValue) {
          languageName = normalizeLanguage(language, aliases);
          node.value = restOfValue;
        }
      }

      const className = `${classPrefix}${languageName}`;

      node.type = "html";
      node.value = `<code class="${className}">${highlightCode(
        languageName,
        node.value
      )}</code>`;
    });
  }
};
