declare module "parse-numeric-range" {
  function parse(range: string): number[];
}

declare module "prismjs/components/index" {
  function loadLanguages(languages: string[]): void;
  export = loadLanguages;
}
type Grammar = {
  title: string;
  require?: string | string[];
  alias?: string | string[];
  aliasTitles?: Record<string, string>;
  owner: string;
  option: string;
};
declare module "prismjs/components" {
  type Components = {
    languages: {
      [key: string]: Grammar;
    };
  };
  const components: Components;
  export = components;
}
declare module "prismjs/components/prism-core";
declare module "unist-util-visit";
