import Prism from "prismjs";
import _ from "lodash";
import loadLanguages from "prismjs/components/index";
import { languages } from "prismjs/components";

import { handleDirectives } from "./directives";

const unsupportedLanguages = new Set();

export function highlightCode(
  language: string,
  code: string,
  lineNumbersHighlight: number[] = []
): string {
  const lang = language.toLowerCase();
  if (lang === "none") {
    return code;
  }
  if (lang === "text" || unsupportedLanguages.has(lang)) {
    return _.escape(code);
  }
  // (Try to) load languages on demand.
  if (!Prism.languages[lang]) {
    try {
      loadLanguages([lang]);
    } catch (e) {
      // Language wasn't loaded so let's bail.
      console.log(e);
      const message = `unable to find prism language '${language}' for highlighting.`;
      if (!unsupportedLanguages.has(lang)) {
        console.warn(message, "applying generic code block");
        unsupportedLanguages.add(lang);
      }
      return _.escape(code);
    }
  }

  const grammar = Prism.languages[lang];

  if (!grammar) {
    return code;
  }

  const highlighted = Prism.highlight(code, grammar, lang);
  const codeSplits = handleDirectives(highlighted, lineNumbersHighlight);

  let finalCode = "";

  const lastIdx = codeSplits.length - 1;
  // Don't add back the new line character after highlighted lines
  // as they need to be display: block and full-width.
  codeSplits.forEach((split, idx) => {
    finalCode += split.highlight
      ? split.code
      : `${split.code}${idx === lastIdx ? "" : "\n"}`;
  });

  return finalCode;
}

const grammarMap: Record<string, Grammar & { lang: string }> = {};

_.forEach(languages, (g, lang) => {
  if (lang === "meta") {
    return;
  }
  const grammar = { ...g, lang };
  grammarMap[lang] = grammar;
  const { alias } = grammar;
  if (!alias) {
    return;
  }
  if (_.isArray(alias)) {
    alias.forEach(a => {
      grammarMap[a] = grammar;
    });
  } else {
    grammarMap[alias] = grammar;
  }
});

export function getTitle(name: string): string | null {
  const lang = name.toLowerCase();
  if (unsupportedLanguages.has(lang)) {
    return null;
  }
  if (!(lang in grammarMap)) {
    unsupportedLanguages.add(lang);
    return null;
  }
  const { title, aliasTitles = {} } = grammarMap[lang];
  const aliasTitle = aliasTitles[lang];
  return aliasTitle ? aliasTitle : title;
}

export function getName(name: string): string {
  const lang = name.toLowerCase();
  if (unsupportedLanguages.has(lang)) {
    return name;
  }
  if (!(lang in grammarMap)) {
    unsupportedLanguages.add(lang);
    return name;
  }
  return grammarMap[lang].lang;
}
