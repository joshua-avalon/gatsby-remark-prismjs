import rangeParser from "parse-numeric-range";
import _ from "lodash";

export type Range = {
  splitLanguage?: string;
  highlightLines?: number[];
  showLineNumbersLocal?: boolean;
  numberLinesStartAt?: number;
};

function parseLineNumber(
  option: string
): Pick<Range, "showLineNumbersLocal" | "numberLinesStartAt"> {
  const options = option.split(":");
  if (options.length !== 2) {
    return {};
  }
  const [key, valueStr] = options;
  const trimValue = valueStr.trim();
  let numberLinesStartAt = -1;
  if (trimValue === "true") {
    numberLinesStartAt = 1;
  } else {
    const valueNum = _.parseInt(trimValue);
    if (_.isSafeInteger(valueNum) && valueNum > 0) {
      numberLinesStartAt = valueNum;
    }
  }
  if (key !== "numberLines" || numberLinesStartAt < 1) {
    return {};
  }
  return { showLineNumbersLocal: true, numberLinesStartAt };
}

export function parseLineNumberRange(language: string): Range {
  if (!language) {
    return {};
  }
  if (language.split("{").length > 1) {
    const [splitLanguage, ...options] = language.split("{");
    // Options can be given in any order and are optional
    let range: Range = { splitLanguage, highlightLines: [] };
    options.forEach(lineOption => {
      const option = lineOption.slice(0, -1);
      // Test if the option is for line hightlighting
      if (rangeParser.parse(option).length > 0) {
        range.highlightLines = rangeParser.parse(option).filter(n => n > 0);
      }
      range = _.merge(range, parseLineNumber(option));
    });
    return range;
  }
  return { splitLanguage: language };
}
