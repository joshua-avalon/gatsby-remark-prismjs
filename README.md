# @joshuaavalon/gatsby-remark-prismjs

Fork of [gatsby-remark-prismjs][gatsby]. Rewritten in TypeScript and fixed some bugs.

Use as your own risk.

[gatsby]: https://www.npmjs.com/package/gatsby-remark-prismjs
